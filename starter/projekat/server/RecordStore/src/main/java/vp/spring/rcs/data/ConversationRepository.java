package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Conversation;


public interface ConversationRepository extends JpaRepository<Conversation, Long>{
  
}
